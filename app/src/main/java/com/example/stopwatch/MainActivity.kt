package com.example.stopwatch

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var toggleStartReset: Boolean = false
    private var togglePauseContinue: Boolean = false
    private var numSeconds: Int? = 0

    val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {
            numSeconds = intent?.getIntExtra("StopWatchCountUpdate", 0)
            stopWatchCountView.setText(numSeconds.toString())
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val intentFilter = IntentFilter()
        intentFilter.addAction("StopWatchCount")
        registerReceiver(broadCastReceiver, intentFilter)

        reset()

        toggleStartResetBtn.setOnClickListener {
            toggleStartReset = !toggleStartReset
            if(toggleStartReset){
                toggleStartResetBtn.text = "Start"
                stopService(Intent(applicationContext, StopWatchService::class.java))
                reset()
            }else{
                toggleStartResetBtn.text = "Reset"
                togglePauseContinueBtn.isEnabled = true
                toggleStartResetBtn.isEnabled = true
                startService(Intent(applicationContext, StopWatchService::class.java))
            }

        }

        togglePauseContinueBtn.setOnClickListener {
            if(togglePauseContinue){
                togglePauseContinueBtn.text = "Pause"
                togglePauseContinueBtn.isEnabled = true
                toggleStartResetBtn.isEnabled = true
                startService(Intent(applicationContext, StopWatchService::class.java)
                    .putExtra("startSeconds", numSeconds))
            }else{
                togglePauseContinueBtn.text = "Continue"
                stopService(Intent(applicationContext, StopWatchService::class.java))
            }
           togglePauseContinue = !togglePauseContinue
        }
    }
    
    private fun reset() {
        togglePauseContinueBtn.isEnabled = false
        togglePauseContinueBtn.text = "Pause"
        togglePauseContinue = false
        toggleStartReset = true
    }
}
