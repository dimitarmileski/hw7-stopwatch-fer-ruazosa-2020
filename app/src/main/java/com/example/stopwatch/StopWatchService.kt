package com.example.stopwatch

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import java.util.*

class StopWatchService : Service() {
    var count: Int = 0
    var timer: Timer? = null
    var timerTask: TimerTask? = null
    override fun onBind(intent: Intent): IBinder {
        TODO("Return the communication channel to the service.")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        count = intent!!.getIntExtra("startSeconds", 0)

        timer = Timer()
        timerTask = object :TimerTask(){
            override fun run() {
                var intentLocal: Intent = Intent()
                intentLocal.setAction("StopWatchCount")

                println("counter = " + count)
                intentLocal.putExtra("StopWatchCountUpdate", count)
                count ++
                sendBroadcast(intentLocal)
            }
        }
        timer?.scheduleAtFixedRate(timerTask, 0, 1000)

        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        timerTask?.cancel()
        super.onDestroy()
    }
}
